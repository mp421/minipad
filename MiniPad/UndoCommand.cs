﻿namespace MiniPad
{
    class UndoCommand: AbstractCommand
    {
        public override bool doIt()
        {
            // This method should never be called
            throw new NoSuchMethodError();
        } // doIt()

        public override bool undoIt()
        {
            // This method should never be called
            throw new NoSuchMethodError();
        } // undoIt()

    }
}
