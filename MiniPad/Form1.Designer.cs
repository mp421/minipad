﻿namespace MiniPad
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fontColorBox = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.selAllBtn = new System.Windows.Forms.Button();
            this.repBtn = new System.Windows.Forms.Button();
            this.findBtn = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.spacingBox = new System.Windows.Forms.ComboBox();
            this.rightJust = new System.Windows.Forms.RadioButton();
            this.centJust = new System.Windows.Forms.RadioButton();
            this.indRight = new System.Windows.Forms.Button();
            this.leftJust = new System.Windows.Forms.RadioButton();
            this.indLeft = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.cutBtn = new System.Windows.Forms.Button();
            this.cpBtn = new System.Windows.Forms.Button();
            this.pasteBtn = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.undBtn = new System.Windows.Forms.Button();
            this.fontHighlightBox = new System.Windows.Forms.ComboBox();
            this.supBtn = new System.Windows.Forms.Button();
            this.subBtn = new System.Windows.Forms.Button();
            this.stBtn = new System.Windows.Forms.Button();
            this.italicBtn = new System.Windows.Forms.Button();
            this.boldBtn = new System.Windows.Forms.Button();
            this.sizeDn = new System.Windows.Forms.Button();
            this.fontSizeBox = new System.Windows.Forms.ComboBox();
            this.sizeUp = new System.Windows.Forms.Button();
            this.fontBox = new System.Windows.Forms.ComboBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.networkingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hostServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectToServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disconnectFromRemoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sendDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.retrieveRemoteDocumentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.autoSyncWithRemoteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // fontColorBox
            // 
            this.fontColorBox.FormattingEnabled = true;
            this.fontColorBox.Location = new System.Drawing.Point(148, 30);
            this.fontColorBox.Name = "fontColorBox";
            this.fontColorBox.Size = new System.Drawing.Size(60, 21);
            this.fontColorBox.TabIndex = 7;
            this.fontColorBox.Text = "Black";
            this.fontColorBox.SelectedIndexChanged += new System.EventHandler(this.fontColorBox_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(2, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(704, 89);
            this.panel1.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.selAllBtn);
            this.panel4.Controls.Add(this.repBtn);
            this.panel4.Controls.Add(this.findBtn);
            this.panel4.Location = new System.Drawing.Point(565, 2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(132, 85);
            this.panel4.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(41, 61);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Editing";
            // 
            // selAllBtn
            // 
            this.selAllBtn.Location = new System.Drawing.Point(53, 28);
            this.selAllBtn.Name = "selAllBtn";
            this.selAllBtn.Size = new System.Drawing.Size(60, 19);
            this.selAllBtn.TabIndex = 3;
            this.selAllBtn.Text = "Select All";
            this.selAllBtn.UseVisualStyleBackColor = true;
            this.selAllBtn.Click += new System.EventHandler(this.selAllBtn_Click);
            // 
            // repBtn
            // 
            this.repBtn.Location = new System.Drawing.Point(53, 3);
            this.repBtn.Name = "repBtn";
            this.repBtn.Size = new System.Drawing.Size(60, 19);
            this.repBtn.TabIndex = 2;
            this.repBtn.Text = "Replace";
            this.repBtn.UseVisualStyleBackColor = true;
            this.repBtn.Click += new System.EventHandler(this.repBtn_Click);
            // 
            // findBtn
            // 
            this.findBtn.BackgroundImage = global::MiniPad.Properties.Resources.find_text;
            this.findBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.findBtn.Location = new System.Drawing.Point(3, 1);
            this.findBtn.Name = "findBtn";
            this.findBtn.Size = new System.Drawing.Size(44, 46);
            this.findBtn.TabIndex = 0;
            this.findBtn.UseVisualStyleBackColor = true;
            this.findBtn.Click += new System.EventHandler(this.findBtn_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.label3);
            this.panel5.Controls.Add(this.spacingBox);
            this.panel5.Controls.Add(this.rightJust);
            this.panel5.Controls.Add(this.centJust);
            this.panel5.Controls.Add(this.indRight);
            this.panel5.Controls.Add(this.leftJust);
            this.panel5.Controls.Add(this.indLeft);
            this.panel5.Location = new System.Drawing.Point(357, 2);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(205, 85);
            this.panel5.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Align";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(56, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Line Spacing";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(76, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Paragraph";
            // 
            // spacingBox
            // 
            this.spacingBox.FormattingEnabled = true;
            this.spacingBox.Location = new System.Drawing.Point(133, 4);
            this.spacingBox.Name = "spacingBox";
            this.spacingBox.Size = new System.Drawing.Size(62, 21);
            this.spacingBox.TabIndex = 5;
            this.spacingBox.Text = "1";
            this.spacingBox.SelectedIndexChanged += new System.EventHandler(this.spacingBox_SelectedIndexChanged_1);
            // 
            // rightJust
            // 
            this.rightJust.AutoSize = true;
            this.rightJust.Location = new System.Drawing.Point(151, 32);
            this.rightJust.Name = "rightJust";
            this.rightJust.Size = new System.Drawing.Size(50, 17);
            this.rightJust.TabIndex = 4;
            this.rightJust.TabStop = true;
            this.rightJust.Text = "Right";
            this.rightJust.UseVisualStyleBackColor = true;
            this.rightJust.CheckedChanged += new System.EventHandler(this.rightJust_CheckedChanged);
            // 
            // centJust
            // 
            this.centJust.AutoSize = true;
            this.centJust.Location = new System.Drawing.Point(96, 32);
            this.centJust.Name = "centJust";
            this.centJust.Size = new System.Drawing.Size(56, 17);
            this.centJust.TabIndex = 3;
            this.centJust.TabStop = true;
            this.centJust.Text = "Center";
            this.centJust.UseVisualStyleBackColor = true;
            this.centJust.CheckedChanged += new System.EventHandler(this.centJust_CheckedChanged);
            // 
            // indRight
            // 
            this.indRight.BackgroundImage = global::MiniPad.Properties.Resources.indent_increase;
            this.indRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.indRight.Location = new System.Drawing.Point(27, 2);
            this.indRight.Name = "indRight";
            this.indRight.Size = new System.Drawing.Size(25, 25);
            this.indRight.TabIndex = 3;
            this.indRight.UseVisualStyleBackColor = true;
            this.indRight.Click += new System.EventHandler(this.indRight_Click);
            // 
            // leftJust
            // 
            this.leftJust.AutoSize = true;
            this.leftJust.Location = new System.Drawing.Point(49, 32);
            this.leftJust.Name = "leftJust";
            this.leftJust.Size = new System.Drawing.Size(43, 17);
            this.leftJust.TabIndex = 2;
            this.leftJust.TabStop = true;
            this.leftJust.Text = "Left";
            this.leftJust.UseVisualStyleBackColor = true;
            this.leftJust.CheckedChanged += new System.EventHandler(this.leftJust_CheckedChanged);
            // 
            // indLeft
            // 
            this.indLeft.BackgroundImage = global::MiniPad.Properties.Resources.indent_decrease;
            this.indLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.indLeft.Location = new System.Drawing.Point(3, 2);
            this.indLeft.Name = "indLeft";
            this.indLeft.Size = new System.Drawing.Size(25, 25);
            this.indLeft.TabIndex = 2;
            this.indLeft.UseVisualStyleBackColor = true;
            this.indLeft.Click += new System.EventHandler(this.indLeft_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.cutBtn);
            this.panel2.Controls.Add(this.cpBtn);
            this.panel2.Controls.Add(this.pasteBtn);
            this.panel2.Location = new System.Drawing.Point(2, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(72, 85);
            this.panel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.label1.Location = new System.Drawing.Point(12, 61);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Clipboard";
            // 
            // cutBtn
            // 
            this.cutBtn.BackgroundImage = global::MiniPad.Properties.Resources.scissors;
            this.cutBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cutBtn.Location = new System.Drawing.Point(47, 29);
            this.cutBtn.Name = "cutBtn";
            this.cutBtn.Size = new System.Drawing.Size(21, 23);
            this.cutBtn.TabIndex = 1;
            this.cutBtn.UseVisualStyleBackColor = true;
            this.cutBtn.Click += new System.EventHandler(this.cutBtn_Click);
            // 
            // cpBtn
            // 
            this.cpBtn.BackgroundImage = global::MiniPad.Properties.Resources.copy;
            this.cpBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.cpBtn.Location = new System.Drawing.Point(47, 0);
            this.cpBtn.Name = "cpBtn";
            this.cpBtn.Size = new System.Drawing.Size(21, 23);
            this.cpBtn.TabIndex = 2;
            this.cpBtn.UseVisualStyleBackColor = true;
            this.cpBtn.Click += new System.EventHandler(this.cpBtn_Click);
            // 
            // pasteBtn
            // 
            this.pasteBtn.BackgroundImage = global::MiniPad.Properties.Resources.Clipboard;
            this.pasteBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pasteBtn.Location = new System.Drawing.Point(3, 1);
            this.pasteBtn.Name = "pasteBtn";
            this.pasteBtn.Size = new System.Drawing.Size(43, 58);
            this.pasteBtn.TabIndex = 0;
            this.pasteBtn.UseVisualStyleBackColor = true;
            this.pasteBtn.Click += new System.EventHandler(this.pasteBtn_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.undBtn);
            this.panel3.Controls.Add(this.fontHighlightBox);
            this.panel3.Controls.Add(this.supBtn);
            this.panel3.Controls.Add(this.subBtn);
            this.panel3.Controls.Add(this.stBtn);
            this.panel3.Controls.Add(this.italicBtn);
            this.panel3.Controls.Add(this.boldBtn);
            this.panel3.Controls.Add(this.sizeDn);
            this.panel3.Controls.Add(this.fontSizeBox);
            this.panel3.Controls.Add(this.sizeUp);
            this.panel3.Controls.Add(this.fontBox);
            this.panel3.Controls.Add(this.fontColorBox);
            this.panel3.Location = new System.Drawing.Point(77, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(277, 84);
            this.panel3.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(123, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Font";
            // 
            // undBtn
            // 
            this.undBtn.BackgroundImage = global::MiniPad.Properties.Resources.Underline1;
            this.undBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.undBtn.Location = new System.Drawing.Point(49, 29);
            this.undBtn.Name = "undBtn";
            this.undBtn.Size = new System.Drawing.Size(24, 23);
            this.undBtn.TabIndex = 6;
            this.undBtn.UseVisualStyleBackColor = true;
            this.undBtn.Click += new System.EventHandler(this.undBtn_Click);
            // 
            // fontHighlightBox
            // 
            this.fontHighlightBox.FormattingEnabled = true;
            this.fontHighlightBox.Location = new System.Drawing.Point(214, 29);
            this.fontHighlightBox.Name = "fontHighlightBox";
            this.fontHighlightBox.Size = new System.Drawing.Size(60, 21);
            this.fontHighlightBox.TabIndex = 8;
            this.fontHighlightBox.Text = "White";
            this.fontHighlightBox.SelectedIndexChanged += new System.EventHandler(this.fontHighlightBox_SelectedIndexChanged);
            // 
            // supBtn
            // 
            this.supBtn.BackgroundImage = global::MiniPad.Properties.Resources.superscript;
            this.supBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.supBtn.Location = new System.Drawing.Point(118, 29);
            this.supBtn.Name = "supBtn";
            this.supBtn.Size = new System.Drawing.Size(24, 23);
            this.supBtn.TabIndex = 8;
            this.supBtn.UseVisualStyleBackColor = true;
            this.supBtn.Click += new System.EventHandler(this.supBtn_Click);
            // 
            // subBtn
            // 
            this.subBtn.BackgroundImage = global::MiniPad.Properties.Resources.subscript;
            this.subBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.subBtn.Location = new System.Drawing.Point(95, 29);
            this.subBtn.Name = "subBtn";
            this.subBtn.Size = new System.Drawing.Size(24, 23);
            this.subBtn.TabIndex = 7;
            this.subBtn.UseVisualStyleBackColor = true;
            this.subBtn.Click += new System.EventHandler(this.subBtn_Click);
            // 
            // stBtn
            // 
            this.stBtn.BackgroundImage = global::MiniPad.Properties.Resources.strikethrough;
            this.stBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.stBtn.Location = new System.Drawing.Point(72, 29);
            this.stBtn.Name = "stBtn";
            this.stBtn.Size = new System.Drawing.Size(24, 23);
            this.stBtn.TabIndex = 7;
            this.stBtn.UseVisualStyleBackColor = true;
            this.stBtn.Click += new System.EventHandler(this.stBtn_Click);
            // 
            // italicBtn
            // 
            this.italicBtn.BackgroundImage = global::MiniPad.Properties.Resources.Indent;
            this.italicBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.italicBtn.Location = new System.Drawing.Point(26, 29);
            this.italicBtn.Name = "italicBtn";
            this.italicBtn.Size = new System.Drawing.Size(24, 23);
            this.italicBtn.TabIndex = 5;
            this.italicBtn.UseVisualStyleBackColor = true;
            this.italicBtn.Click += new System.EventHandler(this.italicBtn_Click);
            // 
            // boldBtn
            // 
            this.boldBtn.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.boldBtn.BackgroundImage = global::MiniPad.Properties.Resources.Bold;
            this.boldBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.boldBtn.Location = new System.Drawing.Point(2, 29);
            this.boldBtn.Name = "boldBtn";
            this.boldBtn.Size = new System.Drawing.Size(24, 23);
            this.boldBtn.TabIndex = 4;
            this.boldBtn.UseVisualStyleBackColor = false;
            this.boldBtn.Click += new System.EventHandler(this.boldBtn_Click);
            // 
            // sizeDn
            // 
            this.sizeDn.BackgroundImage = global::MiniPad.Properties.Resources.decrease_font;
            this.sizeDn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sizeDn.Location = new System.Drawing.Point(246, 2);
            this.sizeDn.Name = "sizeDn";
            this.sizeDn.Size = new System.Drawing.Size(25, 25);
            this.sizeDn.TabIndex = 3;
            this.sizeDn.UseVisualStyleBackColor = true;
            this.sizeDn.Click += new System.EventHandler(this.sizeDn_Click);
            // 
            // fontSizeBox
            // 
            this.fontSizeBox.FormattingEnabled = true;
            this.fontSizeBox.Location = new System.Drawing.Point(161, 4);
            this.fontSizeBox.Name = "fontSizeBox";
            this.fontSizeBox.Size = new System.Drawing.Size(47, 21);
            this.fontSizeBox.TabIndex = 1;
            this.fontSizeBox.Text = "11";
            this.fontSizeBox.SelectedIndexChanged += new System.EventHandler(this.fontSizeBox_SelectedIndexChanged);
            this.fontSizeBox.SelectionChangeCommitted += new System.EventHandler(this.fontSizeBox_SelectionChangeCommitted);
            // 
            // sizeUp
            // 
            this.sizeUp.BackgroundImage = global::MiniPad.Properties.Resources.increase_font;
            this.sizeUp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sizeUp.Location = new System.Drawing.Point(217, 2);
            this.sizeUp.Name = "sizeUp";
            this.sizeUp.Size = new System.Drawing.Size(25, 25);
            this.sizeUp.TabIndex = 2;
            this.sizeUp.UseVisualStyleBackColor = true;
            this.sizeUp.Click += new System.EventHandler(this.sizeUp_Click);
            // 
            // fontBox
            // 
            this.fontBox.FormattingEnabled = true;
            this.fontBox.Location = new System.Drawing.Point(2, 4);
            this.fontBox.Name = "fontBox";
            this.fontBox.Size = new System.Drawing.Size(153, 21);
            this.fontBox.TabIndex = 0;
            this.fontBox.Text = "Microsoft Sans Serif";
            this.fontBox.SelectedIndexChanged += new System.EventHandler(this.fontBox_SelectedIndexChanged);
            this.fontBox.SelectionChangeCommitted += new System.EventHandler(this.fontBox_SelectionChangeCommitted);
            // 
            // panel6
            // 
            this.panel6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel6.BackColor = System.Drawing.SystemColors.Window;
            this.panel6.Location = new System.Drawing.Point(2, 121);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(702, 441);
            this.panel6.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.MenuBar;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.networkingToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(704, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            this.fileToolStripMenuItem.Click += new System.EventHandler(this.fileToolStripMenuItem_Click);
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.newToolStripMenuItem.Text = "New";
            this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.openToolStripMenuItem.Text = "Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.saveToolStripMenuItem.Text = "Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem});
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.cutToolStripMenuItem.Text = "Cut";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.copyToolStripMenuItem.Text = "Copy";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.pasteToolStripMenuItem.Text = "Paste";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.undoToolStripMenuItem.Text = "Undo";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(103, 22);
            this.redoToolStripMenuItem.Text = "Redo";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // networkingToolStripMenuItem
            // 
            this.networkingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hostServerToolStripMenuItem,
            this.connectToServerToolStripMenuItem,
            this.disconnectFromRemoteToolStripMenuItem,
            this.sendDocumentToolStripMenuItem,
            this.retrieveRemoteDocumentToolStripMenuItem,
            this.toolStripSeparator1,
            this.autoSyncWithRemoteToolStripMenuItem});
            this.networkingToolStripMenuItem.Name = "networkingToolStripMenuItem";
            this.networkingToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.networkingToolStripMenuItem.Text = "Networking";
            // 
            // hostServerToolStripMenuItem
            // 
            this.hostServerToolStripMenuItem.Name = "hostServerToolStripMenuItem";
            this.hostServerToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.hostServerToolStripMenuItem.Text = "Host Server";
            this.hostServerToolStripMenuItem.Click += new System.EventHandler(this.hostServerToolStripMenuItem_Click);
            // 
            // connectToServerToolStripMenuItem
            // 
            this.connectToServerToolStripMenuItem.Name = "connectToServerToolStripMenuItem";
            this.connectToServerToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.connectToServerToolStripMenuItem.Text = "Connect to Server";
            this.connectToServerToolStripMenuItem.Click += new System.EventHandler(this.connectToServerToolStripMenuItem_Click);
            // 
            // disconnectFromRemoteToolStripMenuItem
            // 
            this.disconnectFromRemoteToolStripMenuItem.Name = "disconnectFromRemoteToolStripMenuItem";
            this.disconnectFromRemoteToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.disconnectFromRemoteToolStripMenuItem.Text = "Disconnect From Remote";
            this.disconnectFromRemoteToolStripMenuItem.Click += new System.EventHandler(this.disconnectFromRemoteToolStripMenuItem_Click);
            // 
            // sendDocumentToolStripMenuItem
            // 
            this.sendDocumentToolStripMenuItem.Name = "sendDocumentToolStripMenuItem";
            this.sendDocumentToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.sendDocumentToolStripMenuItem.Text = "Send Local Document";
            this.sendDocumentToolStripMenuItem.Click += new System.EventHandler(this.sendDocumentToolStripMenuItem_Click);
            // 
            // retrieveRemoteDocumentToolStripMenuItem
            // 
            this.retrieveRemoteDocumentToolStripMenuItem.Name = "retrieveRemoteDocumentToolStripMenuItem";
            this.retrieveRemoteDocumentToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.retrieveRemoteDocumentToolStripMenuItem.Text = "Retrieve Remote Document";
            this.retrieveRemoteDocumentToolStripMenuItem.Click += new System.EventHandler(this.retrieveRemoteDocumentToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(216, 6);
            // 
            // autoSyncWithRemoteToolStripMenuItem
            // 
            this.autoSyncWithRemoteToolStripMenuItem.Checked = true;
            this.autoSyncWithRemoteToolStripMenuItem.CheckOnClick = true;
            this.autoSyncWithRemoteToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.autoSyncWithRemoteToolStripMenuItem.Name = "autoSyncWithRemoteToolStripMenuItem";
            this.autoSyncWithRemoteToolStripMenuItem.Size = new System.Drawing.Size(219, 22);
            this.autoSyncWithRemoteToolStripMenuItem.Text = "Auto Sync With Remote";
            this.autoSyncWithRemoteToolStripMenuItem.Click += new System.EventHandler(this.autoSyncWithRemoteToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 561);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "MiniPad";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        #region custom static code

        private void InitializeDocument()
        {
            // 
            // document
            // 
            Form1.document = new MiniPad.Document(this);
            Form1.document.AcceptsTab = true;
            Form1.document.Anchor =
                ((System.Windows.Forms.AnchorStyles)
                    ((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                       | System.Windows.Forms.AnchorStyles.Left)
                      | System.Windows.Forms.AnchorStyles.Right)));
            Form1.document.BackColor = System.Drawing.Color.White;
            Form1.document.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            Form1.document.ForeColor = System.Drawing.Color.Black;
            Form1.document.Location = new System.Drawing.Point(0, -2);
            Form1.document.Name = "document";
            Form1.document.Size = new System.Drawing.Size(704, 443);
            Form1.document.TabIndex = 0;
            Form1.document.Text = "";
            //Form1.document.MouseClick += new System.Windows.Forms.MouseEventHandler(Form1.document_MouseClick);
            Form1.document.TextChanged += new System.EventHandler(Form1.document_TextChanged);
            this.panel6.Controls.Add(Form1.document);
        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button pasteBtn;
        private System.Windows.Forms.Button cutBtn;
        private System.Windows.Forms.Button cpBtn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox fontSizeBox;
        private System.Windows.Forms.Button sizeUp;
        private System.Windows.Forms.ComboBox fontBox;
        private System.Windows.Forms.Button sizeDn;
        public System.Windows.Forms.ComboBox fontColorBox;
        private System.Windows.Forms.Button supBtn;
        private System.Windows.Forms.Button subBtn;
        private System.Windows.Forms.Button stBtn;
        private System.Windows.Forms.Button italicBtn;
        private System.Windows.Forms.Button boldBtn;
        private System.Windows.Forms.Button undBtn;
        private System.Windows.Forms.ComboBox fontHighlightBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ComboBox spacingBox;
        private System.Windows.Forms.RadioButton rightJust;
        private System.Windows.Forms.RadioButton centJust;
        private System.Windows.Forms.Button indRight;
        private System.Windows.Forms.RadioButton leftJust;
        private System.Windows.Forms.Button indLeft;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button selAllBtn;
        private System.Windows.Forms.Button repBtn;
        private System.Windows.Forms.Button findBtn;
        private System.Windows.Forms.Panel panel6;
        public static Document document;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem networkingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hostServerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectToServerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sendDocumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem retrieveRemoteDocumentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem autoSyncWithRemoteToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem disconnectFromRemoteToolStripMenuItem;
    }
}
