﻿using System;

namespace MiniPad
{
    public class Item {
        private readonly double value;
        private readonly string name;

        public Item(string n, double v)
        {
            name = n;
            value = v;
        }

        public Double getValue()
        {
            return value;
        }

        public String getName()
        {
            return name;
        }

        public override string ToString()
        {
            // Generates the text shown in the combo box
            return getName();
        }
    }
}
