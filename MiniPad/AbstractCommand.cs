﻿namespace MiniPad
{
    public abstract class AbstractCommand: Undo
    {
        public static CommandManager manager = new CommandManager();

        public abstract bool doIt();
        public abstract bool undoIt();
    }
}
