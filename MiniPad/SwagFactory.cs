﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace MiniPad
{
    [Serializable]
    public class SwagFactory
    {
        public List<Swag> l = new List<Swag>();

        public Swag getSwag(Font f, Color fc, Color bc)
        {
            foreach (Swag m in l.Where(m => m.fc == fc && m.bc == bc && Equals(f, m.f)))
            {
                return m;
            }
            var mc = new Swag(f, fc, bc);
            l.Add(mc);
            return mc;
        }

        public int getObjCount()
        {
            return l.Count;
        }
    }
}