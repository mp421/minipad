﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MiniPad
{
    public class ServerThread
    {
        
        public static Int32 port;
        public static bool listening = false;
        public static Document doc;

        private ServerThread(ref Document d)
        {
            doc = d;
        }
        
        public static void run()
        {
            IPAddress localAddr = IPAddress.Parse("192.168.1.6");

            // TcpListener server = new TcpListener(port);
            TcpListener server = new TcpListener(localAddr, port);

            // Start listening for client requests.
            
            
            server.Start();

            var connections = new List<TcpClient>();

            //TcpClient client = server.AcceptTcpClient();
            
            // Enter the listening loop.
            Boolean clientsConnected = false;
            while (true)
            {
                Thread.Sleep(2000);
                //handle multiple clients
                if (server.Pending())
                {
                    connections.Add(server.AcceptTcpClient());
                    Console.WriteLine("Connected!");
                    clientsConnected = true;
                }
                while (listening)
                {
                    foreach (var client in connections)
                    {
                        if (!client.Connected)
                        {
                            connections.Remove(client);
                            if (connections.Count < 1)
                            {
                                listening = false;
                            }
                        }
                        // Get a stream object for reading and writing
                        NetworkStream stream = client.GetStream();
                        doc.netStream = stream;
                        int i;

                        // Loop to receive all the data sent by the client.
                        if (clientsConnected)
                        {
                            if (Form1.sendPending || Form1.CanAutomaticallySync())
                            {
                                
                                    if (doc.InvokeRequired)
                                    {
                                        doc.Invoke(new MethodInvoker(doc.sendItOut));
                                    }
                                    else
                                    {
                                        doc.sendItOut();
                                    }
                                
                                Form1.sendPending = false;
                            }

                            if (Form1.pullPending || Form1.CanAutomaticallySync())
                            {
                                if (doc.InvokeRequired)
                                    doc.Invoke(new MethodInvoker(doc.bringItIn));
                                else
                                    doc.bringItIn();
                        
                                Form1.sendPending = false;
                            }
                        }
                    }
                }
                //when done listening, close all the connections
                foreach (var client in connections)
                {
                    // Shutdown and end connection
                    client.Close();
                }
            }
        }
    }
}


//// Convert an object to a byte array
//private byte[] ObjectToByteArray(Object obj)
//{
//    if(obj == null)
//        return null;
//    BinaryFormatter bf = new BinaryFormatter();
//    MemoryStream ms = new MemoryStream();
//    bf.Serialize(ms, obj);
//    return ms.ToArray();
//}
//You just need copy this function to your code and send to it the object that you need convert to byte array. If you need convert the byte array to object again you can use the funcion below:

//// Convert a byte array to an Object
//private Object ByteArrayToObject(byte[] arrBytes)
//{
//    MemoryStream memStream = new MemoryStream();
//    BinaryFormatter binForm = new BinaryFormatter();
//    memStream.Write(arrBytes, 0, arrBytes.Length);
//    memStream.Seek(0, SeekOrigin.Begin);
//    Object obj = (Object) binForm.Deserialize(memStream);
//    return obj;
//}
