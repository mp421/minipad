﻿using System;
using System.Drawing;

namespace MiniPad
{
    [Serializable]
    public class Swag : SwagIF //Swag is actually a Flyweight object.
    {
        public Color bc;
        public Font f;

        public Color fc;
        public Single s; //size

        public Swag(Font font, Color fcol, Color bcol)
        {
            f = font;
            fc = fcol;
            bc = bcol;
        }
    }
}