﻿using System;

namespace MiniPad
{
    public class InsertStringCommand: AbstractCommand
    {
        private Document document;  // The document to insert into
        private String strng;       // The string to insert
        private int position;       // The position to insert at

        public InsertStringCommand(Document document, int position, String strng)
        {
            this.document = document;
            this.position = position;
            this.strng = strng;
            manager.invokeCommand(this);
        } // Constructor(Document, int, String)

        public override bool doIt()
        {
            try
            {
                document.insertStringCommand(position, strng);
            }
            catch (Exception e)
            {
                return false;
            } // try
            return true;
        } // doIt()

        public override bool undoIt()
        {
            try
            {
                document.deleteCommand(position, strng.Length);
            }
            catch (Exception e)
            {
                return false;
            } // try
            return true;
        } // undoIt()

    }
}
