﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;

//namespace MiniPad
//{
//    class ReadWriteLock
//    {
//    private int waitingForReadLock = 0;
//    private int outstandingReadLocks = 0;
//    private ArrayList waitingForWriteLock = new ArrayList();
//    private Thread writeLockedThread;
    
//    public void readLock(){
//        try{
//        if(writeLockedThread != null){
//            waitingForReadLock++;
//            while(writeLockedThread != null){
//                wait();
//            }
//            waitingForReadLock--;
//        }
//        outstandingReadLocks++;
//        }finally{
//            Console.WriteLine(""); //just made up something
//            }
//    }
    
//    public void writeLock() throws InterruptedException{
//        Thread thisThread = Thread.currentThread();
//        synchronized(this){
//            if(writeLockedThread == null && outstandingReadLocks == 0){
//                writeLockedThread = thisThread;
//                return;
//            }
//            waitingForWriteLock.add(thisThread);
//        }
//        synchronized(thisThread){
//            while(thisThread != writeLockedThread){
//                thisThread.wait();
//            }
//        }
//        synchronized(this){
//            waitingForWriteLock.remove(thisThread);
//        }
//    }
    
//    public synchronized void done() {
//        if(outstandingReadLocks > 0){
//            outstandingReadLocks--;
//            if(outstandingReadLocks == 0 && waitingForWriteLock.size() > 0){
//                writeLockedThread = (Thread) waitingForWriteLock.get(0);
//                writeLockedThread.notifyAll();
//            }
//        }
//        else if (Thread.currentThread() == writeLockedThread){
//            if(outstandingReadLocks == 0 && waitingForWriteLock.size() > 0){
//                writeLockedThread = (Thread) waitingForWriteLock.get(0);
//                writeLockedThread.notifyAll();
//            }
//            else {
//                writeLockedThread = null;
//                if(waitingForReadLock > 0)
//                    notifyAll();
//            }
//        }
//        else{
//            String msg = "Thread does not have lock";
//            throw new IllegalStateException(msg);
//        }
//    }
//    }
//}
