﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace MiniPad
{
    public class Document : RichTextBox
    {
        public const int default_size = 11;
        public const string default_font = "Microsoft Sans Serif";
        public const int IndentAmount = 49;
        public static int font_size = default_size;
        public static String font_name = default_font;
        public static Font f = new Font(default_font, default_size, FontStyle.Regular);
        public static Color fc = Color.Black;
        public static Color bc = Color.White;
        public static AttributeWrapper a;
        private int currentLocationGlobal;
        public List<int> cutList = new List<int>();
        public List<int> places = new List<int>();
        public string stringGlobal = "";
        public AttributeWrapper wrapperForCut;
        public int indexHelp = 0;
        public Boolean notTextBox = false;
        public CommandManager cm = new CommandManager();
        private Form1 frm;

        public Document(Form1 form)
        {

            newDocument();
            wrapperForCut = new AttributeWrapper(null, new Swag(f, fc, bc));
            places.Insert(0, 0);
            //set the rich text box to our defaults
            Font = f;
            ForeColor = fc;
            BackColor = bc;
            frm = form;
            this.IsAccessible = true;
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            switch (keyData)
            {
                case (Keys.Control | Keys.C):
                    //base.ProcessCmdKey(ref msg, keyData);
                    Copy();
                    //copy();
                    return true;
                case (Keys.Control | Keys.V):
                    notTextBox = true;
                    // anythingChanged();
                    //base.ProcessCmdKey(ref msg, keyData);
                    Paste();
                    //paste();
                    return true;
                case (Keys.Control | Keys.X):
                    // anythingChanged();
                    //base.ProcessCmdKey(ref msg, keyData);
                    Cut();
                    //cut();
                    return true;
                case (Keys.Control | Keys.N):
                    newDocument();
                    return true;
                case (Keys.Control | Keys.Q):
                    Application.Exit();
                    return true;
                case (Keys.Control | Keys.S):
                    Form1.SaveIt();
                    return true;
                case (Keys.Control | Keys.O):
                    Form1.OpenIt();
                    return true;
                case (Keys.Control | Keys.Z):
                    //Undo();
                    cm.undo();
                    return true;
                case (Keys.Control | Keys.Y):
                    //Redo();
                    cm.redo();
                    return true;
                default:
                    //if (char.IsLetterOrDigit((char)keyData) || char.IsPunctuation((char)keyData) ||
                    //    char.IsSeparator((char)keyData) || char.IsSymbol((char)keyData))
                    //{
                    //    places[getPos(SelectionStart)] += 1;
                    //    places[indexHelp] += 1;
                    //    Console.WriteLine("PLACE SET TO: {0}", places[getPos(SelectionStart)]);
                    //}
                    return base.ProcessCmdKey(ref msg, keyData);
            }
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            if (SelectionLength > 0 == false && SelectionStart == TextLength)
                SelectionColor = Form1.colorValues[Form1.colorList.IndexOf(frm.fontColorBox.Text)];
        }


        private void SetSize(int size)
        {

            SelectionFont = new Font(SelectionFont.FontFamily, size);
        }

        public void SetFont(string newFont)
        {

            SelectionFont = new Font(newFont, SelectionFont.Size);

        }

        //takes in the location of which the string is inserted. the wrapper that is related to the string and the list of indexes assocated with that string
        //it inserts the list into the current list of lengths and the wrapper into the current wrappers, it handles if the string is located inside a center of a
        //wrapper. by duplicating the swag object and inserting between the two objects.
        private void PasteWithAttributes(int location, AttributeWrapper anything, List<int> insertedList)
        {
            notTextBox = true;
            int hold = 0;
            for (int i = 0; i < places.Count; i++)
            {
                hold += places.ElementAt(i);
                if (location > hold) continue;
                int diff = hold - location;
                if (diff > 0)
                {
                    places[i] = places[i] - diff;
                    places.Insert(i, diff);
                    a.duplicateWrapper(i, 0);
                }
                a.setWrapper(i, anything, 0);
                places.InsertRange(i, insertedList);
            }
            anythingChanged();
        }

        public void SizeUp()
        {

            SelectionFont = new Font(SelectionFont.FontFamily, SelectionFont.Size + 1);
            //anythingChanged();
        }

        public void SizeDown()
        {
            if (SelectionFont.Size >= 2)
                SelectionFont = new Font(SelectionFont.FontFamily, SelectionFont.Size - 1);
            // anythingChanged();
        }

        public void setSize(int size)
        {

            font_size = size;
            SelectionFont = new Font(SelectionFont.FontFamily, size);
        }

        public static int GetSize()
        {
            return font_size;
        }

        //this method must be called before using currentLocationGlobal or it will be wrong
        private int getPos(int pos)
        {
            if (pos == 0) return 0;
            var count = 0;
            int posi = 0;
            for (int i = 0; i < places.Count; i++)
            {
                count += places[i];
                if (count <= pos) continue;
                posi = i;
                currentLocationGlobal = count;
                break;
            }
            return posi;
        }



        public void cut()
        {
            notTextBox = true;
            var startBeginning = 0;
            var startLast = places[0];
            var count = places[0];
            var startPos = 0;
            for (var i = 1; SelectionStart > count; i++)
            {
                startBeginning = count + 1;
                try
                {
                    count += places[i];
                    startPos = i;
                    var bo = places[i + 1] == 0;
                    if (bo)
                        startBeginning -= 1;
                }
                catch (Exception)
                {
                    Console.WriteLine(@"index out of bounds and it still hasn't found narnia");
                }
            }
            startLast = count;
            var endPos = startPos;
            var endBeginning = SelectionStart + SelectionLength;
            for (var i = startPos + 1; SelectionStart + SelectionLength > count; i++)
            {

                endBeginning = count + 1;
                try
                {
                    count += places[i];
                    endPos = i;
                    var bo = places[i + 1] == 0;
                    if (bo)
                        endBeginning -= 1;
                }
                catch (Exception)
                {
                    Console.WriteLine(@"index out of bounds and it still hasn't found narnia");
                }
            }
            endPos += 1;
            wrapperForCut = a.getForCopy(startPos, endPos, 0);
            a = a.deleteWrapper(startPos, endPos, 0);
            //check indexes
            cutList = places.GetRange(startPos, (endPos - startPos));
            places.RemoveRange(startPos, (endPos - startPos));
            stringGlobal = Text.Substring(SelectionStart, (SelectionLength));
            Text = Text.Substring(0, SelectionStart) + Text.Substring(SelectionStart + SelectionLength);
            a.setText(this, places, 0, 0);
        }

        public bool deleteCommand(int position, int length)
        {
            try
            {
                Text = Text.Substring(0, position) + Text.Substring(position + length);
            }
            catch (Exception ex)
            {

                return false;
            }
            return true;
        }

        public void copy()
        {
            notTextBox = true;
            int startLocation = SelectionStart;
            int endLocation = SelectionLength + SelectionStart;
            int startPos = getPos(SelectionStart);
            int endPos = getPos(endLocation);
            wrapperForCut = a.getForCopy(startPos - 1, endPos, 0);
            // a = a.deleteWrapper(startPos, endPos, 0);
            //check indexes
            cutList = places.GetRange(startPos, (endPos - startPos));
            // places.RemoveRange(startPos, (endPos - startPos));
            stringGlobal = Text.Substring(startLocation, (endLocation - startLocation));
            //Text = Text.Substring(0, startLocation) + Text.Substring(endLocation);
            // a.setText(document, places, 0, 0);
        }

        private void anythingChanged()
        {

            indexHelp += 1;
            var startLocation = SelectionStart;
            var startPos = getPos(startLocation);
            // var temp = currentLocationGlobal - startLocation;
            var endLocation = SelectionLength + startLocation;
            // places[startPos] -= temp;
            if ((!(SelectedText.Length > 1)))
            {
                //places.Insert(startPos + 1, temp);
                //a = a.duplicateWrapper(startPos, 0);
                if (startLocation == TextLength)
                    places.Insert(indexHelp, 0);
                else
                {
                    var startBeginning = 0;
                    var startLast = places[0];
                    var count = places[0];
                    var startPoss = 0;
                    for (var i = 1; SelectionStart > count; i++)
                    {
                        startBeginning = count + 1;
                        try
                        {
                            count += places[i];
                            startPoss = i;
                            var bo = places[i + 1] == 0;
                            if (bo)
                                startBeginning -= 1;
                        }
                        catch (Exception)
                        {
                            Console.WriteLine(@"index out of bounds and it still hasn't found narnia");
                        }
                    }
                    indexHelp = startPoss;

                }
                //f = new Font();
                a = a.setWrapper(indexHelp, new Swag(f, fc, bc));
            }
            else
            {
                selectionPosition();

                //var endPos = getPos(endLocation);
                //temp = currentLocationGlobal - endLocation;
                //places[endPos] -= temp;
                //a = a.deleteWrapper(startPos, endPos, 0);
                //places.RemoveRange(startPos, endPos);
                ////f = new Font();
                //a = a.setWrapper(startPos, new Swag(f, fc, bc));
                //places.Insert(startPos, (endLocation - startLocation));
            }
            //a = (AttributeWrapper) a.atrIF;
            if (!notTextBox)
                a.setText(this, places, 0, 0);

            else notTextBox = false;

        }



        public void selectionPosition()
        {
            var startBeginning = 0;
            var startLast = places[0];
            var count = places[0];
            var startPos = 0;
            for (var i = 1; SelectionStart < count; i++)
            {
                startBeginning = count + 1;
                try
                {
                    count += places[i];
                    startPos = i;
                    var bo = places[i + 1] == 0;
                    if (bo)
                        startBeginning -= 1;
                }
                catch (Exception)
                {
                    Console.WriteLine(@"index out of bounds and it still hasn't found narnia");
                }
            }
            startLast = count;
            var endPos = startPos;
            var endBeginning = SelectionStart + SelectionLength;
            for (var i = startPos; SelectionStart + SelectionLength < count; i++)
            {

                endBeginning = count + 1;
                try
                {

                    count += places[i];
                    endPos = i;
                    var bo = places[i + 1] == 0;
                    if (bo)
                        endBeginning -= 1;
                }
                catch (Exception)
                {
                    Console.WriteLine(@"index out of bounds and it still hasn't found narnia");
                }
            }
            var endLast = count;
            var start = SelectionStart == startBeginning;
            var end = (SelectionStart + SelectionLength) == endLast;

            if (start && end)
            {
                a = a.deleteWrapper(startPos - 1, endPos + 1, 0);
                places.RemoveRange(startPos - 1, endPos - startPos + 2);
                places.Insert(startPos, SelectionLength);

            }
            else if (start)
            {

                a = a.deleteWrapper(startPos - 1, endPos, 0);
                places.RemoveRange(startPos - 1, endPos - startPos + 1);
                places[startPos] = endLast - (SelectionStart + SelectionLength);
                places.Insert(startPos, SelectionLength);

            }
            else if (end)
            {
                a = a.deleteWrapper(startPos, endPos + 1, 0);
                places.RemoveRange(startPos, endPos + 1 - startPos);
                places.Insert(startPos, SelectionLength);
                places[startPos] = SelectionStart - startBeginning;

            }
            else
            {
                a = a.deleteWrapper(startPos, endPos, 0);
                places.RemoveRange(startPos, endPos - startPos);
                places[startPos] = SelectionStart - startBeginning;
                places[startPos + 1] = endLast - SelectionStart - SelectionLength;
                places.Insert(startPos, SelectionLength);

            }


        }


        public void paste()
        {

            if (a == null || places == null) return;
            var startLocation = SelectionStart;
            var startPos = getPos(startLocation);
            a = a.setWrapper(startPos, wrapperForCut, 0);
            //a = wrapperForCut;
            places.InsertRange(startPos, cutList);
            Text = Text.Substring(0, startLocation) + stringGlobal +
                   Text.Substring(startLocation);
            a.setText(this, places, 0, 0); // null exception when trying to paste when having nothing copied
        }

        public bool insertStringCommand(int pos, string s)
        {
            try
            {

                Text = Text.Substring(0, pos) + s +
                       Text.Substring(pos);

            }
            catch (Exception ex)
            {
                return false;

            }
            return true;

        }

        private Thread networkingThread;

        public void clientStart()
        {
            ClientThread.doc = this;
            ClientThread.port = 1337;
            ClientThread.ip = IPAddress.Parse("127.0.0.1");
            ThreadStart ts = new ThreadStart(ClientThread.run);
            networkingThread = new Thread(ts);
            networkingThread.Start();


        }

        public void serverStart()
        {
            ServerThread.doc = this;
            ServerThread.port = 1337;
            ThreadStart ts = new ThreadStart(ServerThread.run);
            networkingThread = new Thread(ts);
            networkingThread.Start();

        }

        //this is for loading from save
        public void getWrapper()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(getWrapper));
            }
            else
            {

                //TODO: An InvalidOperationException is thrown here when using client/server. WHY?!
                lock (this)
                {
                    var ll = TextLength;
                    Select(ll - 1, 1);
                    Font fw = SelectionFont;
                    Color cw = SelectionColor;
                    Color bcw = SelectionBackColor;
                    // var fs =  fontstyle is for the whole box and not selectable via seleciton. needs removed from attribute wrapper as well as size
                    a = new AttributeWrapper(null, new Swag(fw, cw, bcw));
                    a = new AttributeWrapper(a, new Swag(fw, cw, bcw));
                    var first = true;
                    var count = 1;
                    places = new List<int>();
                    for (var i = ll - 1; i > 0; i--)
                    {

                        Select(i - 1, 1);
                        if (!Equals(fw, SelectionFont) || cw != SelectionColor ||
                            bcw != SelectionBackColor)
                        {


                            places.Add(count);
                            count = 1;
                            fw = SelectionFont;
                            cw = SelectionColor;
                            bcw = SelectionBackColor;
                            a = new AttributeWrapper(a, new Swag(fw, cw, bcw));
                        }
                        else count++;
                    }
                    places.Add(count);
                    //a = new AttributeWrapper(a, new Swag(fw, cw, bcw));

                }

            }


        }

        public void newDocument()
        {
            //clear text
            Text = "";

            //reset colors to default
            fc = Color.Black;
            bc = Color.White;

            //reset font to default
            f = new Font(default_font, default_size);

            //reset attribute wrapper
            a = new AttributeWrapper(null, new Swag(f, fc, bc));
        }

        public void indentLeft()
        {
            SelectionIndent -= IndentAmount;
        }

        public void indentRight()
        {
            if ((SelectionIndent + SelectionLength + 25) != Width)
            {
                SelectionIndent += IndentAmount;
            }
        }

        public void setBgColor(Color colorValue)
        {
            SelectionBackColor = colorValue;
            bc = colorValue;
            //anythingChanged();
        }

        public void setFgColor(Color colorValue)
        {
            SelectionColor = colorValue;
            fc = colorValue;
            //f = new Font();
            //anythingChanged();
        }

        public new void SaveFile(string path)
        {
            lock (this)
            {
                var ha = new List<HorizontalAlignment>();
                Stream file = File.Create(path);
                getWrapper();
                try
                {
                    BinaryFormatter bformatter = new BinaryFormatter();
                    bformatter.Serialize(file, places);
                    bformatter.Serialize(file, a);
                    bformatter.Serialize(file, Text);
                    var ss = SelectionStart;
                    var sl = SelectionLength;
                    int i = 0;
                    foreach (Char c in Text)
                    {
                        Select(i, 1);
                        ha.Add(SelectionAlignment);
                        i++;
                    }
                    Select(ss, sl);
                    bformatter.Serialize(file, ha);
                }
                catch
                {
                    Console.WriteLine("Unable to save file.");
                }
                finally
                {
                    file.Close();
                }
            }
        }

        /*class MyTcpListener
        {
            public static void Main()
            {
                try
                {
                    // Set the TcpListener on port 13000.
                    Int32 port = 13000;
                    IPAddress localAddr = IPAddress.Parse("127.0.0.1");

                    // TcpListener server = new TcpListener(port);
                    TcpListener server = new TcpListener(localAddr, port);

                    // Start listening for client requests.
                    server.Start();

                    // Buffer for reading data
                    Byte[] bytes = new Byte[256];
                    String data = null;

                    // Enter the listening loop.
                    while (true)
                    {
                        Console.Write("Waiting for a connection... ");

                        // Perform a blocking call to accept requests.
                        // You could also user server.AcceptSocket() here.
                        TcpClient client = server.AcceptTcpClient();
                        Console.WriteLine("Connected!");

                        data = null;

                        // Get a stream object for reading and writing
                        NetworkStream stream = client.GetStream();

                        int i;

                        // Loop to receive all the data sent by the client.
                        while ((i = stream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            // Translate data bytes to a ASCII string.
                            data = System.Text.Encoding.ASCII.GetString(bytes, 0, i);
                            Console.WriteLine(String.Format("Received: {0}", data));

                            // Process the data sent by the client.
                            data = data.ToUpper();

                            byte[] msg = System.Text.Encoding.ASCII.GetBytes(data);

                            // Send back a response.
                            stream.Write(msg, 0, msg.Length);
                            Console.WriteLine(String.Format("Sent: {0}", data));
                        }

                        // Shutdown and end connection
                        client.Close();
                    }
                }
                catch (SocketException e)
                {
                    Console.WriteLine("SocketException: {0}", e);
                }

                Console.WriteLine("\nHit enter to continue...");
                Console.Read();
            }
        }*/

        public new void LoadFile(string path)
        {
            lock (this)
            {
                Stream file = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                try
                {
                    BinaryFormatter bformatter = new BinaryFormatter();
                    places = bformatter.Deserialize(file) as List<int>;
                    a = bformatter.Deserialize(file) as AttributeWrapper;
                    Text = bformatter.Deserialize(file) as string;
                    var ss = SelectionStart;
                    var sl = SelectionLength;
                    int counts = 0, index = 1;
                    while (a.atrIF != null)
                    {
                        Select(counts, places[places.Count - index]);
                        counts += places[places.Count - index];
                        index++;
                        SelectionFont = ((Swag) (a.swag)).f;
                        SelectionColor = ((Swag) (a.swag)).fc;
                        SelectionBackColor = ((Swag) (a.swag)).bc;
                        a = ((AttributeWrapper) a.atrIF);
                    }
                    var ha = bformatter.Deserialize(file) as List<HorizontalAlignment>;

                    int i = 0;
                    foreach (var h in ha)
                    {
                        Select(i, 1);
                        SelectionAlignment = h;
                        i++;
                    }
                    Select(ss, sl);
                }
                catch
                {
                    Console.WriteLine("Unable to open file");
                }
                finally
                {
                    file.Close();
                }
            }
        }

        public NetworkStream netStream;


        public void sendItOut()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(sendItOut));
            }
            else
            {

                getWrapper();
                // TcpClient tcpClient = new TcpClient("127.0.0.1",1337);
                // tcpClient.Connect("127.0.0.1", 1337);
                //  NetworkStream netStream = tcpClient.GetStream();
                var ha = new List<HorizontalAlignment>();

                // try
                // {
                BinaryFormatter bformatter = new BinaryFormatter();
                bformatter.Serialize(netStream, places);
                bformatter.Serialize(netStream, a);
                bformatter.Serialize(netStream, Text);
                var ss = SelectionStart;
                var sl = SelectionLength;
                int i = 0;
                foreach (Char c in Text)
                {
                    Select(i, 1);
                    ha.Add(SelectionAlignment);
                    i++;
                }
                Select(ss, sl);
                bformatter.Serialize(netStream, ha);
                //XmlSerializer x = new XmlSerializer(typeof(AttributeWrapper));
                //x.Serialize(netStream, a);
                //  }
                /* finally
                     {
                         tcpClient.Close();
                     }*/

            }
        }

        //http://msdn.microsoft.com/en-us/library/vstudio/system.net.sockets.tcplistener?cs-save-lang=1&cs-lang=csharp#code-snippet-2
       
        public void bringItIn()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(bringItIn));
            }
            else
            {
                //TcpClient tcpClient = new TcpClient("127.0.0.1", 1337);
                //tcpClient.Connect("127.0.0.1", 1337);
                // NetworkStream netStream = tcpClient.GetStream();
                var ha = new List<HorizontalAlignment>();

                //try
                //{
                BinaryFormatter bformatter = new BinaryFormatter();
                places = bformatter.Deserialize(netStream) as List<int>;
                a = bformatter.Deserialize(netStream) as AttributeWrapper;
                Text = bformatter.Deserialize(netStream) as string;
                var ss = SelectionStart;
                var sl = SelectionLength;
                int counts = 0, index = 1;
                while (a.atrIF != null)
                {
                    Select(counts, places[places.Count - index]);
                    counts += places[places.Count - index];
                    index++;
                    SelectionFont = ((Swag) (a.swag)).f;
                    SelectionColor = ((Swag) (a.swag)).fc;
                    SelectionBackColor = ((Swag) (a.swag)).bc;
                    a = ((AttributeWrapper) a.atrIF);
                }
                var horiza = bformatter.Deserialize(netStream) as List<HorizontalAlignment>;

                int i = 0;
                foreach (var h in horiza)
                {
                    Select(i, 1);
                    SelectionAlignment = h;
                    i++;
                }
                Select(ss, sl);
                /*}
                    finally
                    {
                        tcpClient.Close();
                    }*/
            }
        }
    }
}

