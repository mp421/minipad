﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MiniPad
{
    [Serializable]
    public class AttributeWrapper : AttributeIF
    {

        private static readonly SwagFactory ff = new SwagFactory();

        public  SwagIF swag;
        public  AttributeIF atrIF;

        public AttributeWrapper(AttributeIF aif, SwagIF swg)
        {
            swag = ff.getSwag(((Swag)swg).f, ((Swag)swg).fc, ((Swag)swg).bc);
            atrIF = aif;
        }
        /*
        public Swag getSwag()
        {
            var f = ((Swag)swag).f;
            var s = ((Swag)swag).s;
            var fc = ((Swag)swag).fc;
            var bc = ((Swag) swag).bc;
            return ff.getSwag(f, fc, bc);
        }*/

        public void setText(RichTextBox rtb, List<int> i, int index, int count)
        {
            if ( count < 0 || index < 0) return;
            if (index <1) return;
            rtb.Select(count, i[index]);
            count += i[index];
            rtb.SelectionFont = ((Swag)swag).f;
            rtb.SelectionColor = ((Swag)swag).fc;
            rtb.SelectionBackColor = ((Swag)swag).bc;
            if (atrIF == null) return;
            ((AttributeWrapper)atrIF).setText(rtb, i, index+1, count);
        }

        //takes in a new swag object and sets it to the index given by level. appends the swig to the middle of the wrapper and returns the entire object
        public AttributeWrapper setWrapper(int level, Swag swig)
        {
           // if (this.atrIF == null) return this;
            AttributeWrapper temp = level > 1 ? ((AttributeWrapper)atrIF).setWrapper(level - 1, swig) : new AttributeWrapper(((AttributeWrapper)atrIF), swig);
            return new AttributeWrapper(temp, swag);
        }

       //deletes the wrappers by indexes after start and before level. it just skips them and returns the attribute wrapper after the end index 0 for level
       public AttributeWrapper deleteWrapper(int start, int end, int level)
       {
           if (atrIF == null) return this;
            if(start < level) return new AttributeWrapper(((AttributeWrapper)atrIF).deleteWrapper(start, end, level++), swag);
            else if (level < end) return ((AttributeWrapper)atrIF).deleteWrapper(start, end, level++);
            else return this;
        }

        //sets a bunch of attribute wrappers in a specified level given a count of zero. should refactor to use just level and not count
       public  AttributeWrapper setWrapper(int level, AttributeWrapper aw, int count)
       {
           
           if (count < level && atrIF != null)//<
           {
               return new AttributeWrapper(((AttributeWrapper) atrIF).setWrapper(level, aw, count++), swag);
           }

            if (atrIF == null)
           {
               return aw;
           }
           else
           {
               aw = Append(aw, ((AttributeWrapper) atrIF));
               return new AttributeWrapper(aw, swag);
              // return aw;
           }
          //reference -> return count < level ? new AttributeWrapper(((AttributeWrapper)atrIF).setWrapper(level, aw, count++), swag) : new AttributeWrapper(this, aw); //Is this completely unreachable?????
          
        }

        // that being that we are grabbing everything that is put into it and throwing it away in order to append it to the current wrapper

        public AttributeWrapper Append(AttributeWrapper usedLater, AttributeWrapper Garbage)
        {
            if ((Garbage == null || usedLater == null) || Garbage.atrIF == null || usedLater.atrIF ==null)
            {
                if (Garbage != null) return new AttributeWrapper(usedLater, Garbage.swag);
            }
            return Garbage.atrIF == null ? new AttributeWrapper(usedLater, swag) : new AttributeWrapper(((AttributeWrapper)Garbage.atrIF).Append(usedLater, ((AttributeWrapper)Garbage.atrIF)),swag);
        }

        //takes the attribute wrapper at the specified level given a count of zero and iterates until it reaches the count and duplicates the fonts at that count
        //refactor to only use level...
       public  AttributeWrapper duplicateWrapper(int level, int count)
        {
           if(atrIF == null)return new AttributeWrapper(this, swag);
            return level > count ? (((AttributeWrapper)atrIF).duplicateWrapper(level, count++)) : (new AttributeWrapper(this, swag));
        }

       //returns an attribute wrapper object of all the attributes between the two given indexes of start and end. level is a count. it just iterates until after the start and before the end
       public AttributeWrapper getForCopy(int start, int end, int level)
       {
           if (atrIF == null) return new AttributeWrapper(null, swag);
            if (start < level) return ((AttributeWrapper)atrIF).getForCopy(start, end, level++);
            else if (level < end+1) return new AttributeWrapper(((AttributeWrapper)atrIF).getForCopy(start, end, level++), swag);
            else return new AttributeWrapper(null, swag);

        }

        public bool isEqualToMe(int level, Swag swig)
        {
            if (level > 0) return ((AttributeWrapper) atrIF).isEqualToMe(level--, swig);
            else return swig == swag;
        }
    }
}
