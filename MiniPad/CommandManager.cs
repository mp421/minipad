﻿using System.Collections.Generic;
using System.Linq;

namespace MiniPad
{
    public class CommandManager
    {
        private int maxHistoryLength = 100;
        
    private LinkedList<AbstractCommand> history = new LinkedList<AbstractCommand>();
    private LinkedList<AbstractCommand> redoList = new LinkedList<AbstractCommand>();

    public void invokeCommand(AbstractCommand command) {
        if (command is UndoCommand) {
            command.undoIt();
            return;
        } // if undo
        if (command is RedoCommand) {
            redo();
            return;
        } // if redo
        if (command.doIt()) {
            // doIt returned true, which means it can be undone
            addToHistory(command);
        } else { // command cannot be undone
            history.Clear();
        } // if
        // After a command that is not an undo or a redo, ensure that
        // the redo list is empty.
        if (redoList.Count > 0)
          redoList.Clear();
    } // invokeCommand(AbstractCommand)

    public void undo() {
        if (history.Count > 0) { // If there are commands in the history
            AbstractCommand undoCommand;
            undoCommand = history.ElementAt(history.Count);
            history.RemoveLast();
            undoCommand.undoIt();
            redoList.AddFirst(undoCommand);
        } // if
    } // undo()

    public void redo() {
        if (redoList.Count > 0) { // If the redo list is not empty
            AbstractCommand redoCommand;
            redoCommand = redoList.ElementAt(0);
            redoList.RemoveFirst();
            redoCommand.doIt();
            history.AddFirst(redoCommand);
        } // if 
    } // redo()

  private void addToHistory(AbstractCommand command) {
        history.AddFirst(command);
        // If size of history has exceded maxHistoryLength, remove
        // the oldest command from the history
        if (history.Count > maxHistoryLength)
          history.RemoveLast();
    } // addToHistory(AbstractCommand)
} //class CommandManager
        
    }

