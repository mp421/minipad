﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;

namespace MiniPad
{
    public partial class Form1 : Form
    {
        private static readonly String baseTitle = "MiniPad";

        private enum Mode
        {
            Standalone,
            Server,
            Client
        };

        public static Boolean sendPending = false;
        public static Boolean pullPending = false;
        public static Boolean autoSync = true;

        private Mode currentMode = Mode.Standalone;

        public static readonly List<String> colorList = new List<String>
        {
            "White",
            "Black",
            "Red",
            "Blue",
            "Yellow",
            "Green",
            "Orange",
            "Purple"
        };

        public static readonly List<Color> colorValues = new List<Color>
        {
            Color.White,
            Color.Black,
            Color.Red,
            Color.Blue,
            Color.Yellow,
            Color.Green,
            Color.Orange,
            Color.Purple
        };

        public Form1()
        {
            InitializeComponent();
            InitializeDocument();
            List<string> fontList;
            try
            {
                //try to pull the list of fonts from the system
                fontList = System.Drawing.FontFamily.Families.Select(font => font.Name).ToList();
            }
            catch
            {
                //List of fonts that we will try to support
                fontList = new List<String>
                {
                    "Microsoft Sans Serif",
                    "Times New Roman",
                    "Arial",
                    "Calibri",
                    "Courier New",
                    "Cambria"
                };
            }
            /**/

            //put the fonts in alphabetical order
            fontList.Sort();

            //add the fonts procedurally to the font box
            foreach (String fontname in fontList)
            {
                fontBox.Items.Add(new Item(fontname, 0));
            }

            //add spacings procedurally
            var spacingList = new List<Double> {1, 1.15, 1.5, 2};
            foreach (Double d in spacingList)
            {
                spacingBox.Items.Add(new Item(d.ToString(), d));
            }

            //add sizes to font size box
            for (int i = 8; i <= 20; i++)
            {
                fontSizeBox.Items.Add(new Item(i.ToString(), i));
            }

            //add colors to color box and highlight box
            for (int i = 0; i < colorList.Count; i++)
            {
                fontColorBox.Items.Add(new Item(colorList[i], i));
                fontHighlightBox.Items.Add(new Item(colorList[i], i));
            }

            ScrollBar vScrollBar1 = new VScrollBar();
            vScrollBar1.Dock = DockStyle.Right;
            vScrollBar1.Scroll += (sender, e) => { panel6.VerticalScroll.Value = vScrollBar1.Value; };
            panel6.Controls.Add(vScrollBar1);

            //center the form on the screen
            Left = (Screen.FromControl(this).Bounds.Width - Width)/2;
            Top = (Screen.FromControl(this).Bounds.Height - Height)/2;

            document.newDocument();
        }

        private void fontColorBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //foreground color selection
            document.setFgColor(colorValues[colorList.IndexOf(fontColorBox.Text)]);
        }

        private void fontSizeBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            //font_size = (int)fontSizeBox.SelectedValue;
            //f = new Font(font_name, font_size);
            document.setSize(Convert.ToInt32(fontSizeBox.Text));
        }

        private void fontSizeBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //font_size = (int)fontSizeBox.SelectedValue;
            document.setSize(Convert.ToInt32(fontSizeBox.Text));
            // f = new Font(font_name, font_size);
           // document.setSize((int)fontSizeBox.SelectedValue);
        }

        private void fontBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            document.SetFont(fontBox.SelectedText);
        }

        private void findBtn_Click(object sender, EventArgs e)
        {

        }

        private void centJust_CheckedChanged(object sender, EventArgs e)
        {
            if (centJust.Checked)
                document.SelectionAlignment = HorizontalAlignment.Center;
        }

        private void leftJust_CheckedChanged(object sender, EventArgs e)
        {
            if (leftJust.Checked)
                document.SelectionAlignment = HorizontalAlignment.Left;
        }

        private void rightJust_CheckedChanged(object sender, EventArgs e)
        {
            if (rightJust.Checked)
                document.SelectionAlignment = HorizontalAlignment.Right;
        }

        private void indRight_Click(object sender, EventArgs e)
        {
            document.indentRight();
        }

        private void indLeft_Click(object sender, EventArgs e)
        {
            document.indentLeft();
        }

        private void fontHighlightBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //background color selection
            document.setBgColor(colorValues[colorList.IndexOf(fontHighlightBox.Text)]);
        }

        private void fontBox_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void spacingBox_SelectedIndexChanged(object sender, EventArgs e) //TODO: Fix line spacing
        {
            try
            {
                Double spacing = Double.Parse(spacingBox.SelectedItem.ToString());
                //Paragraph p = document.Document.Blocks.FirstBlock as Paragraph;
                //p.LineHeight = 10;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveIt();
        }

        public static void SaveIt()
        {
            // Create a SaveFileDialog to request a path and file name to save to.
            var saveFile1 = new SaveFileDialog();

            // Initialize the SaveFileDialog to specify the RTF extention for the file.
            saveFile1.DefaultExt = "*.swag";
            saveFile1.Filter = "SWAG Files|*.swag";

            // Determine whether the user selected a file name from the saveFileDialog. 
            if (saveFile1.ShowDialog() == DialogResult.OK &&
                saveFile1.FileName.Length > 0)
            {
                // Save the contents of the RichTextBox into the file.
                document.SaveFile(saveFile1.FileName);
            }  
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenIt();
        }

        public static void OpenIt()
        {
            // Create an OpenFileDialog to request a file to open.
            var openFile1 = new OpenFileDialog();

            // Initialize the OpenFileDialog to look for RTF files.
            openFile1.DefaultExt = "*.swag";
            openFile1.Filter = "swag Files|*.swag";

            // Determine whether the user selected a file from the OpenFileDialog. 
            if (openFile1.ShowDialog() == DialogResult.OK &&
                openFile1.FileName.Length > 0)
            {
                // Load the contents of the file into the RichTextBox.
                document.LoadFile(openFile1.FileName);
            }
            //AttributeWrapper w = document.getWrapper();
        }

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
            document.newDocument();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void selAllBtn_Click(object sender, EventArgs e)
        {
            document.SelectAll();
            document.Focus();
        }

        private void sizeUp_Click(object sender, EventArgs e)
        {
            document.SizeUp();
            fontSizeBox.Text = Document.GetSize().ToString();
        }

        private void sizeDn_Click(object sender, EventArgs e)
        {
            document.SizeDown();
            fontSizeBox.Text = Document.GetSize().ToString();
        }


        private void boldBtn_Click(object sender, EventArgs e)
        {
            try
            {
                document.SelectionFont = new Font(document.SelectionFont, document.SelectionFont.Style ^ FontStyle.Bold);
            }
            catch
            {
                Console.WriteLine("Failed to set attribute");
            }
        }

        private void italicBtn_Click(object sender, EventArgs e)
        {
            try
            {
                document.SelectionFont = new Font(document.SelectionFont, document.SelectionFont.Style ^ FontStyle.Italic);
            }
            catch
            {
                Console.WriteLine("Failed to set attribute");
            }
        }

        private void undBtn_Click(object sender, EventArgs e)
        {
            try
            {
                document.SelectionFont = new Font(document.SelectionFont, document.SelectionFont.Style ^ FontStyle.Underline);
            }
            catch
            {
                Console.WriteLine("Failed to set attribute");
            }
        }

        private void stBtn_Click(object sender, EventArgs e)
        {
            try
            {
                document.SelectionFont = new Font(document.SelectionFont,
                    document.SelectionFont.Style ^ FontStyle.Strikeout);
            }
            catch
            {
                Console.WriteLine("Failed to set attribute");
            }
        }

        private void subBtn_Click(object sender, EventArgs e)
        {
            //subscript
        }

        private void supBtn_Click(object sender, EventArgs e)
        {
            //superscript
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            document.Cut();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            document.Paste();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            document.Copy();
        }

        private void pasteBtn_Click(object sender, EventArgs e)
        {
            document.Paste();
        }

        private void cutBtn_Click(object sender, EventArgs e)
        {
            document.Cut();
        }

        private void cpBtn_Click(object sender, EventArgs e)
        {
            document.Copy();
        }

        /*private void document_MouseClick(object sender, MouseEventArgs e)
        {
            if (document.SelectionLength > 0 == false && document.SelectionStart == document.TextLength)
                document.SelectionColor = colorValues[colorList.IndexOf(fontColorBox.Text)];
            //if (document.SelectionStart+document.SelectionStart == document.TextLength/500) return;
            //var startBeginning = 0;
            //var startLast = document.places[0];
            //var count = document.places[0];
            //var startPos = 0;
            //for (var i = 1; document.SelectionStart > count; i++)
            //{
            //    startBeginning = count + 1;
            //    try
            //    {
            //        var bo = document.places[i + 1] == 0;
            //        if (bo)
            //            startBeginning -= 1;
            //        count += document.places[i];
            //        startPos = i;
            //    }
            //    catch (Exception)
            //    {
            //        Console.WriteLine(@"index out of bounds and it still hasn't found narnia");
            //    }
            //}
            //document.indexHelp = startPos;
        }*/
        AbstractCommand command;
        private static void document_TextChanged(object sender, EventArgs e)
        {
            //command = (AbstractCommand)new InsertStringCommand(document, document.SelectionStart, document.Text.Substring(document.Text.Length - 1, document.Text.Length));
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void spacingBox_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void repBtn_Click(object sender, EventArgs e)
        {

        }

        private void hostServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switchMode(Mode.Server);
        }

        private void connectToServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switchMode(Mode.Client);
        }

        private void retrieveRemoteDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            pullPending = true;
            //document.bringItIn();
        }

        private void autoSyncWithRemoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AutoSync = autoSyncWithRemoteToolStripMenuItem.Checked;
        }

        public static bool AutoSync { get; set; }

        private void Form1_Load(object sender, EventArgs e)
        {
           switchMode(Mode.Standalone);
        }
        Thread networkingThread;
        private void switchMode(Mode m)
        {
            currentMode = m;
            Text = baseTitle + " [";
            if (networkingThread != null && networkingThread.IsAlive)
            {
                ServerThread.listening = false;
                ClientThread.listening = false;
                networkingThread.Abort();
            }
            switch (currentMode)
            {
                case Mode.Client:
                     try
                     {
                         document.clientStart();
                     }
                    catch (Exception e)
                    {
                        Text += " Standalone ";
                    }
                    finally
                    {  
                         Text += " Client ";                     
                    }
                  
                    break;
                case Mode.Server:
                    try
                    {
                        document.serverStart();

                    }
                    catch (Exception e)
                    {
                        Text += " Standalone ";
                    }
                    finally
                    {  Text += " Server "; }
                  
                    break;
                case Mode.Standalone:
                    Text += " Standalone ";
                    ServerThread.listening = false;
                    break;
            }
            Text += "]";
        }

        private void disconnectFromRemoteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            switchMode(Mode.Standalone);
        }

        private void sendDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            sendPending = true;
        }

        public static bool CanAutomaticallySync()
        {
            return AutoSync;
        }
    }
}