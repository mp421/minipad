﻿using System;
using System.Drawing;

namespace MiniPad
{
    internal class Unshareable
    {
        public Color c;
        public Font f;
        public Single size;

        public Unshareable()
        {
            f = Shareable.f;
            c = Shareable.c;
        }

        public Unshareable(Font ft, Color cr, int sz = 12)
        {
            f = ft;
            c = cr;
            size = sz;
        }

        public void SetSize(Single sz)
        {
            size = sz;
        }
    }
}