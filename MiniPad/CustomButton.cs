﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace MiniPad
{
    internal class CustomButton : Button
    {
        private CustomButton()
        {
            Resize += CustomButton_Resize;
        }

        private void CustomButton_Resize(object sender, EventArgs e)
        {
            if (BackgroundImage == null)
                return;
            var pic = new Bitmap(BackgroundImage, new Size(Width, Height));
            BackgroundImage = pic;
        }
    }
}