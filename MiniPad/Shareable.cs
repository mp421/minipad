﻿using System.Drawing;

namespace MiniPad
{
    public static class Shareable
    {
        public static Font f = new Font("Times New Roman", 11);
        public static Color c = Color.Black;
        public static Brush b = new SolidBrush(c);
    }
}