﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MiniPad
{
    class RedoCommand: AbstractCommand
    {
        public override bool doIt()
        {
            // This method should never be called
            throw new NoSuchMethodError();
        } // doIt()

        public override bool undoIt()
        {
            // This method should never be called
            throw new NoSuchMethodError();
        } // undoIt()

    }
}
