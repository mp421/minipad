﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace MiniPad
{
    public class ClientThread
    {
        public static System.Net.IPAddress ip;
        public static int port;
        public static bool listening = false;
        public static Document doc;

        public ClientThread(ref Document d)
        {
            doc = d;
        }

        public static void run()
        {
            TcpClient client = new TcpClient();
            Console.WriteLine("Connecting.....");

            client.Connect("192.168.1.6", 1337);
            // use the ipaddress as in the server program

            Console.WriteLine("Connected");
            while (true)
            {
                Console.Write("Waiting for a connection... ");
                listening = true;
                String data = null;

                // Get a stream object for reading and writing
                NetworkStream stream = client.GetStream();
                doc.netStream = stream;
                int i;

                // Loop to receive all the data sent by the client.
                while (listening)
                {    
                    if (Form1.pullPending || Form1.CanAutomaticallySync())
                    {
                        if (doc.InvokeRequired)
                         doc.Invoke(new MethodInvoker(doc.bringItIn));
                        else
                            doc.bringItIn();
                        
                        Form1.sendPending = false;
                    }

                    if (Form1.sendPending || Form1.CanAutomaticallySync())
                    {
                       
                            if (doc.InvokeRequired)
                            {
                                doc.Invoke(new MethodInvoker(doc.sendItOut));
                            }
                            else
                            {
                                doc.sendItOut();
                            }
                        
                        Form1.sendPending = false;
                    }

                    

                }
                    
            }
        }
    }
}
